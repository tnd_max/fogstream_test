# Generated by Django 2.0.7 on 2018-07-26 05:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mail_posting', '0003_mail_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='schedule',
            name='task',
            field=models.CharField(max_length=255, null=True, verbose_name='Task'),
        ),
    ]
