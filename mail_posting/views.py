from .models import *
from rest_framework import viewsets
from .serializers import GroupSerializer, MailSerializer
from django.http.response import HttpResponse


def unsubscribe(request):
    if 'mail' in request.GET:
        mail = Mail.get_by_hash(hash_string=request.GET['mail'])
        if mail:
            mail.is_active = False
            mail.save()
            return HttpResponse('User %s successfuly unsubscribed' % mail)
        else:
            return HttpResponse('Can\'t find email')
    return HttpResponse('Please enter email')


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class MailViewSet(viewsets.ModelViewSet):
    queryset = Mail.objects.all()
    serializer_class = MailSerializer
