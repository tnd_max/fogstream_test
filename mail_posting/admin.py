from django.contrib import admin
from .models import *

@admin.register(Mail)
class MailAdmin(admin.ModelAdmin):
    list_display = ('email', 'is_active', 'display_groups')


@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
    list_display = ('subject', 'date', 'group')


@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    list_display = ('schedule', 'mail', 'status')


admin.site.register(Group)

