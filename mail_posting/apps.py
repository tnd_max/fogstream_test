from django.apps import AppConfig


class MailPostingConfig(AppConfig):
    name = 'mail_posting'
