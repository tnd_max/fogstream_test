from django.db import models
from django.template.loader import render_to_string
from django.urls import reverse
from mail_posting.tasks import send_emails
from mailSheduller.celery import app
import base64


class Group(models.Model):
    name = models.CharField(verbose_name="Название", max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Группа'
        verbose_name_plural = 'Группы'


class Mail(models.Model):
    email = models.EmailField(verbose_name="Email", max_length=255)
    is_active = models.BooleanField(verbose_name='Активен?')
    groups = models.ManyToManyField('Group')

    def __str__(self):
        return self.email

    def display_groups(self):
        return ', '.join([group.name for group in self.groups.all()[:3]])

    display_groups.short_description = 'Группы'

    def generate_mail(self):
        return render_to_string('mail_posting/mail.html',
                                {'title': 'ti', 'text': 'osome', 'link': self.generate_unsubscribe_link()})

    def generate_unsubscribe_link(self):
        return "%s?mail=%s" % (reverse('unsubscribe'), self.hash_mail())

    @staticmethod
    def get_by_hash(hash_string):
        email = Mail.unhash_mail(hash_string=hash_string)
        try:
            mail = Mail.objects.get(email=email)
        except Mail.DoesNotExist:
            mail = None
        return mail

    @staticmethod
    def unhash_mail(hash_string=''):
        try:
            result = base64.urlsafe_b64decode(hash_string).decode('utf-8')
        except:
            return ''
        return result

    def hash_mail(self):
        return base64.urlsafe_b64encode(self.email.encode('utf-8')).decode('utf-8')

    class Meta:
        verbose_name = 'Подписчик'
        verbose_name_plural = 'Подписчики'


class Schedule(models.Model):
    subject = models.CharField(verbose_name="Тема", max_length=255)
    text = models.TextField(verbose_name="Текст")
    date = models.DateTimeField(verbose_name="Дата рассылки")
    group = models.ForeignKey('Group', models.CASCADE, verbose_name="Группа")
    task = models.CharField(verbose_name="Task", max_length=255, null=True, blank=True)

    def __str__(self):
        return self.subject

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.task:
            app.control.revoke(self.task)
        else:
            result = send_emails.apply_async((self.group.id,), eta=self.date)
            self.task = result.task_id
        super(Schedule, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        verbose_name = 'Рассписание'
        verbose_name_plural = 'Рассписания'


class Report(models.Model):
    mail = models.ForeignKey('Mail', models.CASCADE, verbose_name="Подписчик")
    schedule = models.ForeignKey('Schedule', models.CASCADE, verbose_name="Рассылка")
    status = models.IntegerField(verbose_name="Статус")

    def __str__(self):
        return self.schedule.subject

    class Meta:
        verbose_name = 'Отчет'
        verbose_name_plural = 'Отчеты'
