from django.urls import include, path
from rest_framework import routers
from mail_posting import views


router = routers.DefaultRouter()
router.register('groups', views.GroupViewSet)
router.register('mail', views.MailViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('unsubscribe', views.unsubscribe, name='unsubscribe'),
]
