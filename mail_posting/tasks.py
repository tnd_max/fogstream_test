from django.core.mail import send_mass_mail
from mailSheduller.celery import app
from mailSheduller import settings
from smtplib import SMTPException


@app.task
def send_emails(group_id):
    from mail_posting.models import Group
    group = Group.objects.get(pk=group_id)
    emails = []
    for mail in group.mail_set.filter(is_active=True):
        emails.append(('Subject', mail.generate_mail(), settings.DEFAULT_FROM_EMAIL, [mail.email]))
    emails = tuple(emails)
    try:
        send_mass_mail(
            emails,
            fail_silently=False,
        )
    except SMTPException as e:
        pass #create report with error
    # create success report
